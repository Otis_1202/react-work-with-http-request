import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import axios from 'axios'


/**
 * Setting Default Config for axios
 */
axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com'
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN'
axios.defaults.headers.post['Content-Type'] = 'application/json'



/**
 * Setting Interceptors
 * This will effect to all request you sent
 * More: https://github.com/axios/axios#interceptors
 * */
axios.interceptors.request.use(request => {
    console.log(request)
    /**
     * Edit request before sent
     * ....
    */

    // Must return request, Otherwise, request will be blocked
    return request
}, error => {
    // This will handler error when request fail to sent (May be by internet connection,...)
    console.log(error)
    // Should return promise reject error though so that we still forward it to our request, so we handler it again with the catch method at your component
    return Promise.reject(error)
})

axios.interceptors.response.use(response => {
    console.log(response)

    // Must return reponse, Otherwise, reponse can be received in the component
    return response
}, error => {
    // This will handler error when receive reponse from server
    console.log(error)
    // Should return promise reject error though so that we still forward it to our request, so we handler it again with the catch method at your component
    return Promise.reject(error)
})

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
