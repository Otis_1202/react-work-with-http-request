import React, { Component, Suspense } from 'react';
import {Route, NavLink, Switch, Redirect} from 'react-router-dom'
import Posts from './Posts/Posts'
// import FullPost from './FullPost/FullPost'
// import NewPost from './NewPost/NewPost'
import asyncComponent from '../../hoc/asyncComponent'

import './Blog.css';

const AsyncNewPost = asyncComponent(() => {
    return import('./NewPost/NewPost')
})

const FullPost = React.lazy(() => import('./FullPost/FullPost'))

class Blog extends Component {

    state = {
        auth: true
    }

    render () {
        return (
            <div className='Blog'>
                <header>
                    <nav>
                        <ul>
                            <li><NavLink to="/" exact activeClassName="link-active">Home</NavLink></li>
                            {/* <li><NavLink to="/posts" exact activeClassName="link-active">Posts</NavLink></li> */}
                            <li><NavLink to={{
                                pathname: '/new-post',
                                hash: '#submit', // go to some id in page of pathname
                                search: '?new=true'
                            }} activeClassName="link-active">New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>

                {/**
                 * <Switch> teel React just load the first route match by URL instead of both
                 * You can use one of two ways below to render (render or component)
                 * <Route path="/" exact render={() => <Posts />} />
                 * <Route path="/" exact component={Posts} />
                 */}
                <Switch>
                    <Route path="/" exact component={Posts} />
                    <Redirect from="/posts" to="/"></Redirect>
                    this.state.auth ? <Route path="/new-post" exact render={(props) => <AsyncNewPost {...props} isAuth={this.state.auth} />}  /> : null
                    <Route path="/:postId" exact 
                        render={(props) => (
                            <Suspense fallback={<div>Loading...</div>}>
                                <FullPost {...props} />
                            </Suspense>
                        )} 
                    />
                    <Route render={() => <h1>404: Not Found</h1>} />
                </Switch>
            </div>
        );
    }
}

export default Blog;