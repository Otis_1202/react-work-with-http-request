import axios from 'axios'

// this FILE will overwrite setting at index.js, because in some component, you will import axios from this file, not from axios package any more

const instance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com'
})


instance.defaults.headers.common['Authorization'] = 'AUTH TOKEN FOR GET POSTS'

// instance.interceptors.request
export default instance